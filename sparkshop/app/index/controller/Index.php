<?php
// +----------------------------------------------------------------------
// | SparkShop 坚持做优秀的商城系统
// +----------------------------------------------------------------------
// | Copyright (c) 2022~2099 http://sparkshop.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://opensource.org/licenses/mit-license.php )
// +----------------------------------------------------------------------
// | Author: NickBai  <876337011@qq.com>
// +----------------------------------------------------------------------

namespace app\index\controller;

use addons\seckill\service\OrderService;
use think\facade\Db;
use think\facade\View;

class Index extends Base
{
    /**
     * pc商城首页
     */
    public function index()
    {
        return View::fetch();
    }
}